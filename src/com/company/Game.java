package com.company;

import static com.company.Board.BOARD_SIZE;

public class Game
{
    public static final int DRAW_BOARD_PERIOD  = 1;
    public void start()
    {
        System.out.println("Welcome to cats & mice simulator!^^");

        System.out.println("Initial status of the board:");

        Board board = new Board();
        board.displayBoard();
        System.out.println("Status after the movement:");
        int i = 0;
        while(!(board.AreAllMouseKilled() || board.IsCheeseEaten())) {
            i++;

            board.moveAll();
            board.killMiceIfPossible();
            board.eatCheeseIfPossible();

            if(i % DRAW_BOARD_PERIOD == 0) {
                System.out.println("Turn " + i +":");
                board.displayBoard();
            }
        }
    }
}
