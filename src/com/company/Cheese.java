package com.company;

public class Cheese extends BoardObject {

    public Cheese(int positionX, int positionY, int number) {
        super(positionX, positionY, number);
    }

    public void move(){
            System.out.println("Cheese position: " + positionX + "," + positionY);
        }
}
