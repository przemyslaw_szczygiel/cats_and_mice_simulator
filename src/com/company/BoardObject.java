package com.company;

public abstract class BoardObject
{
    protected int positionX;
    protected int positionY;
    protected int number;

    public BoardObject(int positionX, int positionY, int number) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.number = number;
    }


    public abstract void move();

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }


    public void setNumber(int number) {
        this.number = number;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getNumber() {
        return number;
    }

}
