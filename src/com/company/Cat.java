package com.company;

import java.util.Random;

public class Cat extends Animal
{

    public static final int MAX_CATS = 4;  //maksymalna ilosc kotow na tablicy (stała)

    public Cat(int positionX, int positionY, int number)
    {
        super(positionX, positionY, number);
    }


    public void move() {
        int x = 0;
        int y = 0;
        Random generator = new Random();

        MoveCat[] moveCat = MoveCat.values();
        MoveCat randomMovement; // aktualna pozycja na planszy

        do {
        randomMovement = moveCat[generator.nextInt(5)];

            switch (randomMovement) {
                case TOP: // kot porusza sie do gory
                    x = positionX;
                    y = positionY - 1;
                    break;

                case BOTTOM: // kot porusza sie na dol
                    x = positionX;
                    y = positionY + 1;
                    break;

                case LEFT:  // kot porusza sie w lewo
                    x = positionX - 1;
                    y = positionY;
                    break;

                case RIGHT: //kot porusza sie w prawo
                    x = positionX + 1;
                    y = positionY;
                    break;

                case NULL_STEP: // kot nie porusza sie
                    x = positionX;
                    y = positionY;

            }

        }
        while (x < 0 || x >= Board.BOARD_SIZE || y < 0 || y >= Board.BOARD_SIZE);
        System.out.println("Cat " + number + ": " + positionX + ", " + positionY + " -> " + x + ", " + y);
        positionX = x;
        positionY = y;

    }

    public void info(){}

}
