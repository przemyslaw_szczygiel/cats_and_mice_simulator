package com.company;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public enum MoveMouse {
    TOP, BOTTOM, LEFT, RIGHT, TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, NULL_STEP;

    public static List<MoveMouse> basicMoves = Arrays.asList(TOP, BOTTOM, LEFT, RIGHT, NULL_STEP);

    public static List<MoveMouse> allMoves = Arrays.asList(TOP, BOTTOM, LEFT, RIGHT, TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, NULL_STEP);

    public static MoveMouse getRandomBasicMove(){
        return basicMoves.get(new Random().nextInt(basicMoves.size()));
    }

    public static MoveMouse getRandomAllMove(){
        return allMoves.get(new Random().nextInt(allMoves.size()));
    }
}
