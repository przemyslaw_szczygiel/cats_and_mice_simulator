package com.company;

import java.util.Random;

public class Mouse extends Animal
{

    public static final int MAX_MICE = 5;

    public Mouse(int positionX, int positionY, int number) {  // konsruktor mysz dziedziczący po obiekcie Animal
        super(positionX, positionY, number);
    }

    public void move() // to jest metoda losowo poruszajaca mysz na planszy:
    {

        int x = 0, y = 0;

        Random generator = new Random(); // zainicjalizowanie generatora losowania

        MoveMouse[] moveMouse = MoveMouse.values(); // to jest lista krokow na planszy

        MoveMouse randomMovement; // aktualna pozycja na planszy

        do {
        randomMovement = moveMouse[generator.nextInt(9)];  // losowanie krawedz

            switch (randomMovement) {
                case TOP: // kot porusza sie do góry
                    x = positionX;
                    y = positionY - 1;
                    break;
                case BOTTOM:  // kot porusza sie w dol
                    x = positionX;
                    y = positionY + 1;
                    break;
                case LEFT: // kot porusza sie w lewo
                    x = positionX - 1;
                    y = positionY;
                    break;
                case RIGHT: // kot porusza sie w prawo
                    x = positionX + 1;
                    y = positionY;
                    break;
                case TOPLEFT: // góra-lewo
                    x = positionX - 1;
                    y = positionY - 1;
                    break;
                case TOPRIGHT: // góra-prawo
                    x = positionX + 1;
                    y = positionY - 1;
                    break;
                case BOTTOMRIGHT: //dół-prawo
                    x = positionX + 1;
                    y = positionY + 1;
                    break;
                case BOTTOMLEFT: //dół-lewo
                    x = positionX - 1;
                    y = positionY + 1;
                    break;
                case NULL_STEP: // mysz nie porusza sie
                    x = positionX;
                    y = positionY;
            }
        }
        while (x < 0 || x >= Board.BOARD_SIZE || y < 0 || y >= Board.BOARD_SIZE);
        positionX = x;
        positionY = y;

    }

    public void info(){}
}
