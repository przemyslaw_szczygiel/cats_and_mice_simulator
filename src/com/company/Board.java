package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {

    public static final int BOARD_SIZE = 11;
    public static final int MAX_CATS = 3;
    public static final int MAX_MICE = 10;

    // deklaracja i tworzenie tablicy z obiektami objects;
    BoardObject objects[] = new BoardObject[MAX_CATS + MAX_MICE + 1];


    public Board() {

        int x = 0, y = 0;
        Random generator = new Random(); // zainicjalizowanie generatora losowania

        Edge[] edges = Edge.values(); // to jest lista krawedzi planszy

        Edge currentEdge; // aktualna krawedz planszy
        for (int i = 0; i < MAX_CATS; i++) {

            currentEdge = edges[generator.nextInt(3)];  // losowanie krawedzi planszy

            switch (currentEdge) {
                case TOP:
                    x = generator.nextInt(BOARD_SIZE - 1);
                    y = 0;
                    break;
                case BOTTOM:
                    x = 0;
                    y = BOARD_SIZE - 1;
                    break;
                case LEFT:
                    x = 0;
                    y = generator.nextInt(BOARD_SIZE - 1);
                    break;
                case RIGHT:
                    x = BOARD_SIZE - 1;
                    y = generator.nextInt(BOARD_SIZE - 1);
                    break;
            }

            objects[i] = new Cat(x, y, i + 1);
            System.out.println("Cat " + (i + 1) + ": " + x + ", " + y + ".");
        }


        for (int i = 0; i < MAX_MICE; i++) {
            currentEdge = edges[generator.nextInt(3)];  // losowanie krawedzi planszy

            switch (currentEdge) {
                case TOP:
                    x = generator.nextInt(BOARD_SIZE - 1);
                    y = 0;
                    break;
                case BOTTOM:
                    x = 0;
                    y = BOARD_SIZE - 1;
                    break;
                case LEFT:
                    x = 0;
                    y = generator.nextInt(BOARD_SIZE - 1);
                    break;
                case RIGHT:
                    x = BOARD_SIZE - 1;
                    y = generator.nextInt(BOARD_SIZE - 1);
                    break;
            }

            objects[MAX_CATS + i] = new Mouse(x, y, i + 1);
            System.out.println("Mouse " + (i + 1) + ": " + x + ", " + y + ".");
        }
        objects[MAX_CATS + MAX_MICE] = new Cheese(5, 5, 1);
    }

    // funkcja ktora szuka obiekty na planszy
    public List<BoardObject> findObjectAt(int x, int y) {
        List<BoardObject> list = new ArrayList<>();

        for (int i = 0; i < (MAX_CATS + MAX_MICE + 1); i++) {
            if ((objects[i] != null) && (objects[i].getPositionX() == x) && (objects[i].getPositionY() == y)) {
                list.add(objects[i]);
            }
        }
        return list;
    }


    public void displayBoard() {

        //funkcja która wyswietla planszę
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (findObjectAt(i, j).size() == 0) {
                    System.out.print(".");
                } else {
                    List<BoardObject> objectOnCurPosition = findObjectAt(i, j);
                    char objChar = 'S'; // zmienna przechowujaca znak (obiekt) ktory ma byc wyswietlony na planszy
                    for (int k = 0; k < objectOnCurPosition.size(); k++) {
                        if (objectOnCurPosition.get(k) instanceof Cat) {
                            objChar = 'K';
                            break;
                        } else if (objectOnCurPosition.get(k) instanceof Mouse) {
                            objChar = 'M';
                        }
                    }
                    System.out.print(objChar);
                }
            }
            System.out.println();
        }
    }

    public void moveAll() {
        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) {
                objects[i].move();
            }
        }
    }


    public void killMiceIfPossible() { // funkcja która po wykonaniu ruchu sprawdza czy na jednym polu jest kot i mysz; jesli tak mysz jest zabita

        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (findObjectAt(i, j).size() != 0) {
                    List<BoardObject> objectOnCurPosition = findObjectAt(i, j);
                    int k;
                    for (k = 0; k < objectOnCurPosition.size(); k++) {
                        if (objectOnCurPosition.get(k) instanceof Cat) {
                            break;

                        }
                    }
                    if (k != objectOnCurPosition.size()) {
                        for (k = 0; k < objectOnCurPosition.size(); k++) {
                            if (objectOnCurPosition.get(k) instanceof Mouse) {
                                killMouse(objectOnCurPosition.get(k).getNumber());

                            }
                        }
                    }
                }
            }
        }
    }

    public void killMouse(int number) {
        for (int i = 0; i < (MAX_CATS + MAX_MICE + 1); i++) {
            if ((objects[i] instanceof Mouse) && (objects[i].getNumber() == number)) {
                objects[i] = null;
                System.out.println("Mouse number " + number + " was KILLED!!!");
            }
        }
    }

// funkcja zabijajaca kota jesli stana na nim dwie myszy


    public void eatCheeseIfPossible() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (findObjectAt(i, j).size() != 0) {
                    List<BoardObject> objectOnCurPosition = findObjectAt(i, j);
                    int k;
                    for (k = 0; k < objectOnCurPosition.size(); k++) {
                        if (objectOnCurPosition.get(k) instanceof Mouse) {
                            break;

                        }
                    }
                    if (k != objectOnCurPosition.size()) {
                        for (k = 0; k < objectOnCurPosition.size(); k++) {
                            if (objectOnCurPosition.get(k) instanceof Cheese) {
                                eatCheese(objectOnCurPosition.get(k).getNumber());

                            }
                        }
                    }
                }
            }
        }
    }

    public void eatCheese(int number) {
        for (int i = 0; i < (MAX_CATS + MAX_MICE + 1); i++) {
            if ((objects[i] instanceof Cheese) && (objects[i].getNumber() == number)) {
                objects[i] = null;
                System.out.println("Cheese was EATEN!!!");
            }
        }
    }

    public boolean AreAllMouseKilled() {

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] instanceof Mouse ) {
                return false;
            }
            }
        System.out.println("Cats won!:D");
        return true;
    }

    public boolean IsCheeseEaten(){
        for (int i = 0; i < objects.length; i++) {
            if (objects[i] instanceof Cheese) {
                return false;
            }
        }
        System.out.println("Mice won!^^");
        return true;
    }
}